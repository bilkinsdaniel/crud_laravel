<p align="center"><h4>## Como testar o sistema de cadastro (online)</h4></p>

<h6># Rodando o sistema:</h6>
1 - acesse o link: http://www.ipsistemasweb.com.br/teste/public/

2 - acesse o menu cadastro/clientes, a partir dai poderá inserir, editar, visualizar e excluir os registros.

<p align="center"><h4>## Como testar o sistema de cadastro (offline - maquina local)</h4></p>

<h6># Baixar e instalar o Xampp:</h6>
1 - Abra a página de download do XAMPP. Para fazê-lo, acesse https://www.apachefriends.org/download.html em navegador de internet no computador,  Desça até o cabeçalho "7.2.8 / PHP 7.2.8". Ela está localizada perto do final da lista dos downloads.

2 - startar o servidor Php e Mysql do xampp.

3 - acessar o phpMyAdmin e importar o arquivo "laravel.sql"(está na raiz do projeto) que é a base dos dados do 

<h6># Clonar o projeto:</h6>
1 - abra o CMD e acesse a pasta C:\xampp\htdocs

2 - utilize o comando para clonar o projeto: git clone https://gitlab.com/bilkinsdaniel/crud_laravel.git

<h6># Rodando o sistema:</h6>
3 - depois acesse um navegador (verifique se o apache e o mysql estao startados) e cole o link: http://127.0.0.1//teste/public/

2 - acesse o menu cadastro/clientes, a partir dai poderá inserir, editar, visualizar e excluir os registros.

<p align="center"><h4>## Como utilizar/instalar o projeto do sistema de cadastro</h4></p>

<h6># Clonar o projeto:</h6>
1 - abra o CMD e acesse a pasta C:\xampp\htdocs

2 - utilize o comando para clonar o projeto: git clone https://gitlab.com/bilkinsdaniel/crud_laravel.git

<h6># Baixar e instalar o Xampp e composer:</h6>
3 - Abra a página de download do XAMPP. Para fazê-lo, acesse https://www.apachefriends.org/download.html em navegador de internet no computador,  Desça até o cabeçalho "7.2.8 / PHP 7.2.8". Ela está localizada perto do final da lista dos downloads.

	"Nota importante: como o Laravel exige a versão 7.13 ou superior do PHP, cuidado para não instalar acidentalmente uma versão antiga do XAMPP."

4 - startar o servidor Php e Mysql do xampp.

5 - acessar o phpMyAdmin e importar o arquivo "laravel.sql"(está na raiz do projeto) que é a base dos dados do projeto.

6 - baixe e instale o composer, acesse: https://getcomposer.org/download/

<h6># Rodando o sistema:</h6>
7 - depois acesse a pasta criada com o seguinte comando: cd C:/xampp/htdocs/crud_cadastro

8 - dentro da pasta do projeto você deve executar o seguinte comando: composer install

9 - ainda na pasta do projeto inicie o laravel com o comando: php artisan serve

10 - por fim acesse um navegador e cole o link: http://127.0.0.1:8000

	"Obs.: se o sistema nao iniciar, recomendo renomear o arquivo ".env.example" para ".env", apos isso startar o laravel atraves do comando 'php artisan serve'"

11 - se as imagens salvar nao aparecerem, vá ao diretório do projeto, entre na pasta public e apague a pasta "storage"

12 - windows: através do cmd acesse a pasta do projeto e execute o comando: php artisan storage:link 
	 servidor linux: acesse(via terminal) a pasta public dentro da pasta do projeto (ex.: public_html/meu_projeto/public/) e execute o comando: ln -s ~/public_html/meu_projeto/storage/app/public/ storage
	"Obs.: esse comando irá criar o link simbolico da pasta de storage do sistema"