<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clientes;

class ClienteController extends Controller
{
    private $objCliente;

    public function __construct(){
        $this->objCliente = new Clientes();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = $this->objCliente->all();
        return view("index", compact('cliente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file("foto")->isValid()){
            $filename_foto = $request->foto->store('fotos');
        }else{
            $filename_foto = "";
        }

        $cad = $this->objCliente->create([
            'nome'=>$request->nome,
            'email'=>$request->email,
            'telefone'=>$request->telefone,
            'foto'=>$filename_foto
        ]);

        if ($cad){

            return redirect('clientes');

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = $this->objCliente->find($id);
        return view('show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = $this->objCliente->find($id);
        return view('create', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file("foto") !== null && $request->file("foto")->isValid()){
            $this->objCliente->where(['id'=>$id])->update([
                'nome'=>$request->nome,
                'email'=>$request->email,
                'telefone'=>$request->telefone,
                'foto'=>$request->foto->store('fotos')
            ]);
        }else{
            $this->objCliente->where(['id'=>$id])->update([
                'nome'=>$request->nome,
                'email'=>$request->email,
                'telefone'=>$request->telefone
            ]);
            
        }
        return redirect('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = $this->objCliente->destroy($id);
        return($del)?"sim":"não";
    }
}
