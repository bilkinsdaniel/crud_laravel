@extends("templates.template_menu")

@section('content')

    <h1 class="mt-4">@if(isset($cliente)) Editar Cliente @else Novo Cliente @endif</h1>
    <div class="alert alert-secondary" style="height: 63px">
        <div class="float-right">
            <a href="{{url("clientes")}}">
                <button type="button" class="btn btn-primary" style="width: 100px">Voltar</button>
            </a>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-plus mr-1"></i>Informações do Cliente</div>
        <div class="card-body">
            @if(isset($cliente)) 
                <form name="formEdit" id="formEdit" method="POST" enctype="multipart/form-data" action="{{url("clientes/$cliente->id")}}">
                @method('PUT')
            @else
                <form name="formCad" id="formCad" method="POST" enctype="multipart/form-data" action="{{url("clientes")}}">
            @endif
                    <div class="form-group">
                        @csrf
                        <label for="nome">Nome</label>
                        <input class="form-control" type="text" name="nome" id="nome" placeholder="Nome" value="{{$cliente->nome ?? ''}}" required>
                        
                        <br>

                        <label for="email">E-mail</label>
                        <input class="form-control" type="email" name="email" id="email" placeholder="seu@email.com" value="{{$cliente->email ?? ''}}" required>
                        
                        <br>

                        <label for="telefone">Telefone</label>
                        <input class="form-control" type="text" name="telefone" id="telefone" value="{{$cliente->telefone ?? ''}}" required>
                        
                        <br>
                        <label for="foto">Foto</label>
                        <br>
                        @if(isset($cliente))
                            <img src="{{ asset('storage/'.$cliente->foto) }}" class="img-fluid">
                        @endif
                        
                        <input class="form-control" type="file" name="foto" id="foto" accept="image/*" value="{{$cliente->foto ?? ''}}" @if(!isset($cliente)) required @endif>

                        <br>
                        <div class="alert alert-secondary" style="height: 63px">
                            <div class="float-right">
                                <input type="submit" class="btn btn-success" value="@if(isset($cliente)) Salvar @else Cadastrar @endif">
                            </div>
                        </div>
                        
                        
                    </div>
                </form>
        </div>
    </div>
@endsection