@extends("templates.template_menu")

@section('content')

    <h1 class="mt-4">Lista de Clientes</h1>

    <div class="alert alert-secondary" style="height: 63px">
        <div class="float-right">
            <a href="{{url("clientes/create")}}">
                <button type="button" class="btn btn-success" style="width: 100px">Novo</button>
            </a>
        </div>
    </div>
    
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Clientes</div>
        <div class="card-body">
            <div class="table-responsive">
                @csrf
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">Foto</th>
                            <th scope="col">Nome</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Telefone</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cliente as $clientesItem)
                            <tr>
                                <td><img src="{{ asset('storage/'.$clientesItem->foto) }}" height="100" width="100"></td>    
                                <td>{{$clientesItem->nome}}</td>
                                <td>{{$clientesItem->email}}</td>
                                <td>{{$clientesItem->telefone}}</td>
                                <td>
                                    <div class="dropdown show">
                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Ações
                                        </a>
                                      
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{url("clientes/$clientesItem->id")}}">
                                                <i class="fa fa-search" aria-hidden="true"></i> Visualizar
                                            </a>
                                            <a class="dropdown-item" href="{{url("clientes/$clientesItem->id/edit")}}">
                                                <i class="fa fa-address-card" aria-hidden="true"></i> Editar
                                            </a>
                                            <a class="dropdown-item js-del" href="{{url("clientes/$clientesItem->id")}}">
                                                <i class="fa fa-trash" aria-hidden="true"></i> Deletar
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable(
                {
                    "language": {
                        "lengthMenu": "_MENU_ resultados por página",
                        "zeroRecords": "Nenhum registro encontrado",
                        "info": "Exibindo de _START_ até _END_ de _TOTAL_ registros",
                        "infoEmpty": "Nenhum registro encontrado",
                        "infoFiltered": "(Filtrados de _MAX_ registros)",
                        "search": "Pesquisar:",
                        "paginate": {
                            "next": "Próximo",
                            "previous": "Anterior",
                            "first": "Primeiro",
                            "last": "Último"
                        }
                    }
                }
            );
        });
        //OPÇÕES DE TRADUÇÃO
        //LINK CDN
        //https://cdn.datatables.net/plug-ins/1.10.21/i18n/Portuguese-Brasil.json
    </script>
    <script src="{{url('assets/js/javascript.js')}}"></script>
        
@endsection