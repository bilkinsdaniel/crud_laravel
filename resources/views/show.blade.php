@extends("templates.template_menu")

@section('content')

    <h1 class="mt-4">Visualizar</h1>
    <div class="alert alert-secondary" style="height: 63px">
        <div class="float-right">
            <a href="{{url("clientes")}}">
                <button type="button" class="btn btn-primary" style="width: 100px">Voltar</button>
            </a>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Dados do Cliente</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                <strong>
                                    Id
                                </strong>
                            </td>
                            <td>
                                {{$cliente->id}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    Nome
                                </strong>
                            </td>
                            <td>
                                {{$cliente->nome}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    Email
                                </strong>
                            </td>
                            <td>
                                {{$cliente->email}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    Telefone
                                </strong>
                            </td>
                            <td>
                                {{$cliente->telefone}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    Foto
                                </strong>
                            </td>
                            <td>
                                <img src="{{ asset('storage/'.$cliente->foto) }}" height="100" width="100">
                            </td>
                        </tr>                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection